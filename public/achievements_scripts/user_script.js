// To set the names of the uploaded images on the storage.
$(document).ready(function() { 
	$("#newGame").submit(function( event ) {

		var name = $("#newGameName").val();
		name = name.trim().toLowerCase().replace(/(\s)/g, "-").replace(/[*+~.()'"!:@]/g, "") + "-";

		$("#newLogo").attr("name", name + "logo");

		$("#newImage1").attr("name", name + "img1");

		$("#newImage2").attr("name", name + "img2");

		$("#newImage3").attr("name", name + "img3");
	});
});

// Shows the name of the file uploaded.
function setName(field) {

	if (field.files.length > 0) {

		$(field).after("<small>File " + field.files[0].name + " uploaded.</small>");
	}
}


function deleteGame(row) {
	$.post("/company",
		{
		id: row.dataset.id,
		delete: true
		}
	);
}

function updateGame(row) {

	var toPost = {
		id: row.dataset.id,
		edit: true,
		name: $("#newGameName").val(),
		release: $("#newRelease").val(),
		genre: $("#newGenre").val(),
		plataform: $("#newPlataform").val(),
		gameMode: $("#newGameMode").val(),
		price: $("#newPrice").val(),
		description: $("#description").val(),
		costs: $("#newCosts").val(),

		directedBy: $("#directedBy").val(),
		writtenBy: $("#writtenBy").val(),
		stars: $("#stars").val(),
		music: $("#music").val(),
		artDirection: $("#artDirection").val(),

		videoURL: $("#newGame").find("#newVideo").val()
	}
		
	$.post("/company", toPost);

}


function fillModal(button) {

	var url = "/companyajax?id=" + button.dataset.id;

	$.ajax({
		url: url,
		dataType: "JSON",
		type: 'GET'
		}).done(function(game) {


			$("#newGame").find("#newGameName").val(game.name);
			$("#newGame").find("#newRelease").val(game.release);
			$("#newGame").find("#newGenre").val(game.genre);
			$("#newGame").find("#newPlataform").val(game.plataform);
			$("#newGame").find("#newGameMode").val(game.gameMode);
			$("#newGame").find("#newPrice").val(game.price);
			$("#newGame").find("#description").val(game.description);
			$("#newGame").find("#newCosts").val(game.costs);
			
			$("#newGame").find("#directedBy").val(game.cast.directedBy);
			$("#newGame").find("#writtenBy").val(game.cast.writtenBy);
			$("#newGame").find("#stars").val(game.cast.stars);
			$("#newGame").find("#music").val(game.cast.music);
			$("#newGame").find("#artDirection").val(game.cast.artDirection);
			
			$("#newGame").find("#newVideo").val(game.videoURL);


			$("#editGame").attr("data-id", button.dataset.id);
	});
}