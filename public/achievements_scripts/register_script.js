// Generate User Name
function generateUserCompany(name) {
	
	var company = $("#companyName")[0].value.trim().toLowerCase().replace(/\s/g, ".") + "-";
	var newUser = company + name.trim().toLowerCase().replace(/\s/g, "."); 

	$("#userName")[0].value = newUser;
	
}



// Company Validation
function companyCheck(name) {

		if (name.trim().toLowerCase() == "atari") {
		
		$("#companyHelp").removeClass("sr-only");
	}	else {
		$("#companyHelp").addClass("sr-only");
	}

}

// Email Validation
function emailCheck(email) {

var patt = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;

	if (patt.test(email) == false) {
		
		$("#emailHelp").removeClass("sr-only");

		$("#email").keyup(function(){
			if (patt.test($("#email").val()) == false) { //check this part

				$("#emailHelp").removeClass("sr-only");
			} else { $("#emailHelp").addClass("sr-only")
			return;}})}
	else {
		$("#emailHelp").addClass("sr-only");
		}
}


// Confirm Email Validation

function emailConfirmCheck(emailConfirm) {

	if (emailConfirm != $("#email").val()) {
		$("#emailConfirmHelp").removeClass("sr-only");

		$("#emailConfirm").keyup(function(){
			if ($("#emailConfirm").val() == $("#email").val()) {
				$("#emailConfirmHelp").addClass("sr-only");
			}
		})

	} else {
		$("#emailConfirmHelp").addClass("sr-only");
	}
}

// Password validation

var passwordVal = 0;

function passwordStrenght(n) {

	if (n < 5) {
		console.log("Weak password");
		
	} else { 
			if (n >= 5 && n <= 10) {
				console.log("Normal password");

		} else {
			if (n > 10) {
				console.log("Strong password");
			}
		}
	}
		
}

function passwordCheck(pass) {

	var passwordVal = 0;

	for (var i = 0; i < pass.length; i++) {

		if (/[a-z]/i.test(pass[i])) {
			passwordVal = passwordVal + 1;

		} else {
			if (/[0-9]/.test(pass[i])) {
			
				passwordVal = passwordVal + 2;

			} else {
				passwordVal = passwordVal + 4;
			};
		};
		
	};

	passwordStrenght(passwordVal);
};

// Confirm Password Validation

function passwordConfirmCheck(passConfirm) {

	if (passConfirm != $("#password").val()) {
		$("#passwordConfirmHelp").removeClass("sr-only");

		$("#passwordConfirm").keyup(function(){
			if ($("#passwordConfirm").val() == $("#password").val()) {
				$("#passwordConfirmHelp").addClass("sr-only");
			}
		})

	} else {
		$("#passwordConfirmHelp").addClass("sr-only");
	}
}

//********************************************
function blockForm() {
	$("fieldset").atrr("disabled", "true");
}


//*****************************************
