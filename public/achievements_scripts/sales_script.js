
// var games to manage the data

var games = {};

$(document).ready(function(){

	$.ajax({
		url: "/salesajax",
		dataType: "JSON",
		type: 'GET'
		}).done(function(jsonGames) {

			games = jsonGames;

			chartSales(games);

			chartSalesModal(games);
		});


});

// Fill the chart
function chartSales(game){

	var table = "<tr><th>Date</th><th>Units</th></tr></thead>";

	for (i = 0; i < games.length; i++) {

		if (games[i].sales.sold == undefined) {

			break
		}

		if ($("#gameSelect")[0].value == games[i].name) {

			for (j = 0; j < Object.keys(games[i].sales.sold).length; j++) {
				
				table += "<tr id='R" + (j + 1) + "'><td>" + Object.keys(games[i].sales.sold)[j] + "</td><td>" + Object.values(games[i].sales.sold)[j] + "</td></tr>";
				
			}
			
		}

	}

	return $("#tableSales").html(table);
	
}

// Fill the chart on modal
function chartSalesModal(game){

	var table = "<tr><th>Date</th><th>Units</th></tr>";

	for (i = 0; i < games.length; i++) {

		if (games[i].sales.sold == undefined) {

			break
		}

		if ($("#gameSelect")[0].value == games[i].name) {

			
			for (j = 0; j < Object.keys(games[i].sales.sold).length; j++) {
				
				table += "<tr id='R" + (j) + "'><td>" + Object.keys(games[i].sales.sold)[j] + "</td><td>" + Object.values(games[i].sales.sold)[j] + "</td><td><i data-date='" + Object.keys(games[i].sales.sold)[j] + "' class='far fa-trash-alt' onclick='deleteSale(this)'></i></td></tr>";
				
			}
			
		}

	} 

	return $("#tableSalesModal").html(table)

}


// add key/value sale on modal to future storage

function addSale(date, units) {
	
	var key = date.value;
	var value = parseInt(units.value);

	for (var i = 0; i < games.length; i++) {

		if ($("#gameSelect")[0].value == games[i].name) {

			if (games[i].sales.sold == undefined) {

				games[i].sales.sold = {};
			}

			games[i].sales.sold[key] = value;
		}
	}

	chartSalesModal(games);

	clearCells(date, units);

}


function clearCells(date, units) {
	date.value = "";
	units.value = "";
}

function deleteSale(row) {

	var key = row.dataset.date;

	for (i = 0; i < games.length; i++) {

		if ($("#gameSelect")[0].value == games[i].name) {

			delete games[i].sales.sold[key];
		}
	}

	chartSalesModal(games);
}

function postSales() {

	for (i = 0; i < games.length; i++) {

		if ($("#gameSelect")[0].value == games[i].name) {
		
			$.post("/sales", games[i].sales);
		}
	}
	
}
