window.chartColors = [
    'rgb(255, 99, 132)',    //red
    'rgb(54, 162, 235)',    //blue
    'rgb(75, 192, 192)',    //green
    'rgb(255, 159, 64)',    //orange
    'rgb(153, 102, 255)',   //purple
    'rgb(255, 205, 86)',    //yellow
    'rgb(201, 203, 207)'    //grey
]

var datos ;

$.ajax({
    url : "/gamesajax/",
    type : "get",
    //async: false,
    success : function(games) {
    datos = games;


        var UChart = document.getElementById('UChart').getContext('2d');
        var gamesChart = new Chart(UChart, {
            type: 'line',
            data: {
                //labels: Object.keys(datos.games[0].sales.sold),
                datasets: [{/*
                    label: datos.games[0].name,                         // Conserve for test purpose
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: Object.values(datos.games[0].sales.sold),
                    fill: false,
                },{
                    label: datos.games[0].name,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: Object.values(datos.games[0].sales.sold),
                    fill: false,
                },{
                    label: datos.games[0].name,
                    backgroundColor: window.chartColors.green,
                    borderColor: window.chartColors.green,
                    data: [
                        201456,
                        350123,
                        784569,
                        124569,
                        451038,
                        467851,
                    ],
                    fill: false,
                },{
                    label: datos.games[0].name,
                    backgroundColor: window.chartColors.orange,
                    borderColor: window.chartColors.orange,
                    data: [
                        501231,
                        245781,
                        154523,
                        279842,
                        225469,
                        498751,
                    ],
                    fill: false,
                },{
                    label: datos.games[0].name,
                    backgroundColor: window.chartColors.grey,
                    borderColor: window.chartColors.grey,
                    data: [
                        303124,
                        421045,
                        491230,
                        598413,
                        610234,
                        652134,
                    ],
                    fill: false,
                */}]
            },


            options: {

                responsive: true,
                title: {
                    display: true,
                    text: 'Earnings per month',
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true,
                },

                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                           labelString: 'Month',
                           },
                        ticks: {
                                source: "auto",
                            },

                    }],
                    yAxes: [{
                        ticks: {
                            //suggestedMin: 100000,
                            //suggestedMax: 800000,
                            min: 0, },
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: '$',
                        }
                    }]
                }
            }
        });

        fillChart();

        // Fill the chart

        function fillChart() {

            // Sets the labels. 
            var labelCount = 0;

            var labels = [];

            // Looks for the game who has more sales
            for (i = 0; i < datos.games.length; i++) {

                if (datos.games[i].sales.sold == undefined) {

                    break;
                }

                if (Object.keys(datos.games[i].sales.sold).length > labelCount) {

                    var labelCount = Object.keys(datos.games[i].sales.sold).length;
                }
            }
                // Actually sets the labels
            for (i = 1; i <= labelCount; i++) {
                
                labels.push(i);

            }

            gamesChart.data.labels = labels;
            

            // Sets the data for game

                // Clear the "undefined"
            gamesChart.data.datasets = [];
           

            for (i = 0; i < datos.games.length; i++) {

                if (datos.games[i].sales.sold == undefined) {

                    break;
                }
                
                var newDataSet = {
                    label: datos.games[i].name,
                    backgroundColor: window.chartColors[i],
                    borderColor: window.chartColors[i],
                    data: Object.values(datos.games[i].sales.sold),
                    fill: false
                }
                
                gamesChart.data.datasets.push(newDataSet);
            }

            gamesChart.update();

        }

         
            
    },
});