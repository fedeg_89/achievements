window.chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
};

// ***** Games Chart ***** //

var datos ;

var query = document.getElementById("gameName").dataset.id;
var url = "/gameajax?id=" + query;

$.ajax({
    url: url,
    type: 'GET',
    //async: false,
    success : function(games) {
        datos = games;

    // Calculate the total sold
    var sales = [Object.values(datos.sales.sold)[0]*datos.price];
        for (var i = 1; i < Object.values(datos.sales.sold).length; i++) {
           sales[i] = (Object.values(datos.sales.sold)[i]*datos.price) + sales[i - 1];
        };

    // Add the breakeven
    function punto() {
        for (var j = 0; j < sales.length; j++) {
            if (sales[j] >= datos.costs) {
                var res = {
                    x : Object.keys(datos.sales.sold)[j],
                    y : sales[j]
                };
                return res;
            };
        };
    };

    // The chart
    var DTChart = document.getElementById('statsChart').getContext('2d');
    var usersChart = new Chart(DTChart, {
       type: 'line',
                data: {
                    labels: Object.keys(datos.sales.sold),
                    datasets: [{
                        label: datos.name + " sales",
                        backgroundColor: window.chartColors.green,
                        borderColor: window.chartColors.green,
                        data: sales,
                        fill: false,
                    },{
                        label: "Breakeven",
                        backgroundColor: window.chartColors.yellow,
                        borderColor: window.chartColors.yellow,
                        pointRadius: 10,
                        pointHoverRadius: 25,
                        data: [{
                            x: punto().x,
                            y: punto().y
                        }, ]
                    }]
                },
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: 'Sales'
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false,
                    },
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Month'
                            }
                        }],
                        yAxes: [{
                            ticks: {
                               // suggestedMin: 800000,
                                //suggestedMax: 5000000,
                                min: 0, },
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Earnings',
                            }
                        }]
                    }
                }
            })

    },
});