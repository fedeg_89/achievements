function generateUser(name) {
	
	var company = $("#companyName")[0].textContent;
	company = company.toLowerCase().replace(/\s/g, ".") + "-";
	var newUser = company + name.trim().toLowerCase().replace(/\s/g, "."); 
	
	$("#userName")[0].value = newUser;
	
}

function generateUserModal(name) {
	
	var company = $("#companyName")[0].textContent;
	company = company.toLowerCase().replace(/\s/g, ".") + "-";
	var newUser = company + name.trim().toLowerCase().replace(/\s/g, "."); 
	
	$("#userNameModal")[0].value = newUser;
	
}

function deleteUser(row) {
	$.post("/roles",
		{
		id: row.dataset.id,
		delete: true
		}
	).done(function(data) {
		
		if (JSON.parse(data).status == "ok") {

			$("[data-id='" + row.dataset.id + "']").remove();
			
		}
	});
}

function editUser(button) {

	var toPost = {
		id: button.dataset.id,
		edit: true,
		name: $("#nameModal").val(),
		userName: $("#userNameModal").val(),
		password: $("#passwordModal").val(),
	}

	if ($("#editUser").find("#adminModal")[0].checked) {
		toPost.admin = true;
	} else {
		toPost.admin = false;
	}

	if ($("#editUser").find("#dataEntryModal")[0].checked) {
		toPost.dataEntry = true;
	} else {
		toPost.dataEntry = false;
	}

	if ($("#editUser").find("#marketingModal")[0].checked) {
		toPost.marketing = true;
	} else {
		toPost.marketing = false;
	}


	$.post("/roles",toPost).done(function(data) {
		
		if (JSON.parse(data).status == "ok") {

		// print on the screen once the file is saved on database

			var edited = '<div class="col-sm-3">' +
				'<span>' + toPost.name + '</span></div>' +
				'<div class="col-sm-3">' + 
				'<span>' + toPost.userName + '</span></div><div class="col-sm-3">';

				if (toPost.dataEntry) {
					edited += '<span>Data entry</span>';
				}

				if (toPost.marketing) {
					edited += '<span>Marketing</span>';
				}

				if (toPost.amdin) {
					edited += '<span>Admin</span>';
				}

				edited += '</div><div class="col-sm-3"><i class="far fa-trash-alt" data-id="' + button.dataset.id + '" onclick="deleteUser(this)"></i>' + 
					'<i class="far fa-edit" data-toggle="modal" data-target="#editUser" data-id="' + button.dataset.id + '" onclick="fillModal(this)"></i></div>';
		
			$("#" + button.dataset.id).html(edited);
			
		}
	});

}

//fill the modal with the actual information

function fillModal(button) {

	var url = "/rolesajax?id=" + button.dataset.id;

	$.ajax({
			url: url,
			dataType: "JSON",
			type: 'GET'
			}).done(function(users) {


	$("#editUser").find("#nameModal").val(users.name);
	$("#editUser").find("#userNameModal").val(users.userName);

	if (users.admin) {
		$("#editUser").find("#adminModal")[0].checked = true;
	}
	
	if (users.marketing) {
		$("#editUser").find("#marketingModal")[0].checked = true;
	}

	if (users.dataEntry) {
		$("#editUser").find("#dataEntryModal")[0].checked = true;
	}

	});

	$("#saveUser").attr("data-id", button.dataset.id);
}

// Autocheck other roles when the user checks admin

function adminCheck(check) {

	if (check == true) {
		$("#roles_form")[0].marketing.checked = true;
		$("#roles_form")[0].marketing.disabled = true;

		$("#roles_form")[0].dataEntry.checked = true;
		$("#roles_form")[0].dataEntry.disabled = true;

	} else if (check == false) {
		$("#roles_form")[0].marketing.disabled = false;
		$("#roles_form")[0].dataEntry.disabled = false;
	};	

};

