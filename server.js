//// The modules
const express = require("express");
const hbs = require("hbs");
var bodyParser = require('body-parser');
//var cookieParser = require('cookie-parser');
var session = require('express-session');
var multer = require('multer');
//var upload = multer({ dest: '/images/' });
var bcrypt = require('bcryptjs');

//// The models
var {Company} = require("./db/models");
var {User} = require("./db/models");
var {Game} = require("./db/models");
var {Sales} = require("./db/models");
var {Genre} = require("./db/models");
var {Plataform} = require("./db/models");
var {GameMode} = require("./db/models");


//// Multer options to upload
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/imagesGames')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + ".jpg");
  }
});
 
var upload = multer({ storage: storage });

//// The settings
var app = express();

hbs.registerPartials(__dirname + "/views/partials");
app.set("view engine", "hbs");
app.use(express.static(__dirname + "/public"));
app.set('case sensitive routing', false);

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
//app.use(cookieParser());
app.use(session({
    secret: 'somerandonstuffs',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 600000
    }
}));

/*app.use((req, res, next) => {
    if (req.session.cookie && !req.session.user) {
		req.session.destroy(function(err) {
		});        
    }
    next();
});*/


// Middleware function to check for logged-in users
var sessionChecker = (req, res, next) => {
    if (!req.session.user) {
      return res.redirect('/login');
    } else {
        next();
    }    
};

// ROUTER //

app.get("/", function(req, res) {
	
	res.render("home.hbs", {
		session: req.session.user
	});
});


app.get("/gameajax", function(req, res) {

//**** PENDING: secure url

	var game = Game.findById(req.query.id, "sales price costs name").populate("sales");
	game.then(function(game) {
		
		res.send(game);
	});
});

app.get("/games/:game", function(req, res) {

	var url = req.params.game;

	var game = Game.findOne({url: url}).populate("company", "name").exec(function (err, game){

		res.render("games.hbs", {
			session: req.session.user,

			name: game.name,
			company: game.company,
			genre: game.genre,
			release: game.release.getFullYear(),
			description: game.description,
			cast: game.cast,
			videoURL: game.videoURL,
			url: game.url,
			id: game._id,

			totalSold: game.totalSold,
			earnings: game.earnings,
			price: game.price,
			costs: game.costs
		});

	});
});

app.get("/logout", function(req, res) {
	req.session.destroy(function(err) {
  		res.redirect("/");
	});
});

app.route("/login")
	.get(function(req, res) {
		if (req.session.user) {
			res.redirect("user.hbs");
		} else {
			res.render("login.hbs");
		}
	
})

	.post(function(req, res) {

		var posted = req.body;
		
		user = User.findOne({userName: posted.userName}).populate("company", "name url").then(function (user) {

			if (!user) {
				res.redirect('/login');

			} else {
				//**** PENDING: server side valiations
				bcrypt.compare(posted.password, user.password, function(err, pass) {

					if (!pass) {
						res.redirect('/login');
					} else {

						req.session.user = user;
						res.redirect('/' + user.company.url);
					}

				});
			}
		});
	});


app.get("/rolesajax", function(req, res) {
//**** PENDING: secure url

	var roles = User.findById(req.query.id);
	roles.then(function(roles) {
		
		res.send(roles);
	});
});

app.route("/roles")
	.get(sessionChecker, function(req, res) {

		var company = Company.findById(req.session.user.company);
		
		company.then(function (company){
			var users = User.find({company : company._id});
			users.then(function (users) {


				res.render("roles.hbs", {
					session: req.session.user,

					companyName: company.name,
					userName: users.name,
					
					id: users._id,
					users: users,
					dataEntry: users.dataEntry,
					marketing: users.marketing,
					admin: users.admin
				});

			});

		})

	})

	.post(function(req, res) {
		posted = req.body;

		if (!posted.delete && !posted.edit) {
			//**** PENDING: server side validations

			bcrypt.genSalt(10, function(err, salt) {
				bcrypt.hash(posted.password, salt, function(err, hash) {

					posted.password = hash;

					var user = new User(posted);
					user.company = req.session.user.company;
					
					user.save(function (err) {
						if (err) {
							console.log(err);
							return;
							}

						Company.findById(user.company, function(err, company) {
							company.users.push(user._id);
							company.save();
						});
						//**** PENDING: avoid redirect on post requests

						//res.send(app.route("/roles").get());
						res.redirect("/roles");
					});
				});
			});
		} else if (posted.delete) {
			User.findById(posted.id, function(err, user) {
				user.remove();

				Company.findById(req.session.user.company, "users", function(err, company) {
					company.users.pull(posted.id);
					company.save();
					
					
				});
				//**** PENDING: avoid redirect on post requests. Optimizate this
				res.send('{"status" : "ok"}');
				//res.redirect("/roles");

			});
		} else if (posted.edit) {

			//**** PENDING: server side validations
			bcrypt.genSalt(10, function(err, salt) {
				bcrypt.hash(posted.password, salt, function(err, hash) {

				posted.password = hash;

				var toUpdate = {
					name: posted.name,
					userName: posted.userName,
					password: posted.password,
					admin: posted.admin,
					marketing: posted.marketing,
					dataEntry: posted.dataEntry
				}

				User.findOneAndUpdate({_id : posted.id}, toUpdate, {new: true, useFindAndModify: false}, function(err, saved) {
				if (err) {console.log(err)}
				});
			
				//**** PENDING: avoid redirect on post requests. Optimizate this
				res.send('{"status" : "ok"}');
				//res.redirect("/roles");
				
				});
			});
		}
});

app.get("/salesajax", function(req, res) {
//**** PENDING: secure url

	var games = Game.find({company: req.session.user.company}, "name sales").populate("sales");
	games.then(function(games) {
		
		res.send(games);	
	});
});


app.get("/sales", sessionChecker, function(req, res) {
	
	var company = Company.findById(req.session.user.company);
		
		company.then(function (company){
			var games = Game.find({company: company._id}).populate("sales");
			games.then(function (games) {
				
				res.render("sales.hbs", {
					session : req.session.user,
					games: games
				});
				
			});

		});
});

app.post("/sales", function(req, res) {

	var posted = (req.body);

	var sale = Sales.findById(posted._id, function (err, sale) {
	
		sale.sold = posted.sold;
		sale.save(function(err, sale) {

			var game = Game.findById(sale.game).populate("sales").exec(function(err, game) {

				game.addTotalSold();
				game.save();

				//**** PENDING: avoid redirect on post requests. Optimizate this
				res.redirect("/sales");
			})
		});

	});
});

app.route("/register")
	.get(function(req, res) {
		if (req.session.user) {
			res.redirect("user.hbs");
		} else {
			res.render("register.hbs");
		}
		
	})

	.post(function(req, res) {
		posted = req.body;
		
		//**** PENDING: server side validations

		bcrypt.genSalt(10, function(err, salt) {
			bcrypt.hash(posted.password, salt, function(err, hash) {

				posted.password = hash;

				var company = new Company({
					name: posted.name,
					address: posted.address,
					country: posted.country,
					city: posted.city,
					state: posted.state,
					zip: posted.zip
				});

				company.addUrl();

				company.save(function(err) {

					if(err){
						
					}
										
					newUser = new User({
						name: posted.fullName,
						userName: posted.userName,
						email: posted.email,
						password: posted.password,
						admin: true,
						company: company._id
						});
					
					newUser.save();
					company.users = newUser._id;
					company.save(function(err) {

						if(err){
						
						}

						//this doesn't works. Check it
						req.session.user = newUser.populate("company", "name url");
						
						//**** PENDING: avoid redirect on post requests. Optimizate this
						res.redirect('/roles');

						   
					});

				});
			});
		});

	});



app.get("/search", function(req, res) {
//**** PENDING ****//

	res.render("search.hbs");
});

app.get("/gamesajax", function(req, res) {

	var games = Company.findById(req.session.user.company, "games")
	.populate({
		path: 'games',
		populate: { path: 'sales' }
  });
	games.then(function(game) {
		
		res.send(game);
	});
});


app.get("/companyajax", function(req, res) {
//**** PENDING: secure url

	var game = Game.findById(req.query.id).populate("company", "url");
	game.then(function(game) {
		
		res.send(game);
	});
});

app.route("/:company")
	.get(function(req, res) {

		company = req.params.company.toLowerCase()

		Company.findOne({url: company}, function (err, company){

			if (company == null) {
				
				return res.redirect("/");
			}

			var games = Game.find({company : company._id}).populate("company", "name -_id");
				
			games.then(function(games) {
								
				res.render("user.hbs", {
					session: req.session.user,
					companyName: company.name,
					games: games
					//release: games.release.toLocaleDateString("en-EN", { year: 'numeric', month: 'short'}) ?????
				});
			});
		});
	})

	.post(upload.any(), function(req, res, next) {
		var posted = req.body;

		if (!posted.delete) {
		//**** PENDING: check the whole POST

			posted.genre = posted.genre.split(", ");
			posted.plataform = posted.plataform.split(", ");
			posted.gameMode = posted.gameMode.split(", ");
			posted.company = req.session.user.company._id;
			posted.cast = {
				"Directed by": posted.directedBy,
				"Written by": posted.writtenBy,
				"Stars": posted.stars,
				"Music": posted.music,
				"Art Direction": posted.artDirection
			};

			if (!posted.edit) {

				var game = new Game(posted);

				game.addUrl();

				game.save();
					

				var sales = new Sales ({
					game: game._id,
					sold: {}
				});

				sales.save(function(err, sales) {

					game.sales = sales._id;
					game.save();
				});
						
				
				Company.findById(req.session.user.company._id, function(err, company) {

					company.games.push(game._id);
					company.save();

				});

				//**** PENDING: avoid redirect on post requests. Optimizate this
				res.redirect("/" + req.session.user.company.url);
				
			};
		

			} else if (posted.edit) {

				Game.findOneAndUpdate({_id : posted.id}, posted, {new: true, useFindAndModify: false}, function(err, saved) {
				
				//**** PENDING: avoid redirect on post requests. Optimizate this
				res.redirect("/" + req.session.user.company.url);

				});
			


		} else if (posted.delete) {
			Game.findById(posted.id, function(err, game) {
				game.remove();

				Company.findById(req.session.user.company, "games", function(err, company) {
					company.games.pull(posted.id);
					company.save();
					
					
				});

				//**** PENDING: avoid redirect on post requests. Optimizate this
				res.redirect("/" + req.session.user.company.url);


			});
		} 

		
	});



// Helper for Handlebars: a "if" for rendering

hbs.registerHelper('ifCond', function(v1, v2, options) {
  if(v1 === v2) {
    return options.fn(this);
  }
  return options.inverse(this);
});




app.listen(8088, function(){
	console.log("Server started on port 8088");
});

