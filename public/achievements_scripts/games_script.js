//Fading in the cast list

function renderNextItem() {

	for (var i = 1; i < $("#castList").children().length - 1; i++) {

		if ($("#castList li:eq(" + i + ")").css("visibility") == "hidden") {
		
		$("#castList li:eq(" + i + ")").css("visibility", "visible");
		$("#castList li:eq(" + i + ")").css("display", "none");
		
		$("#castList li:eq(" + i + ")").fadeIn(1000); 

		
		setTimeout(renderNextItem, 1000);

		return;}
		
	}

	$("<li style='visibility: visible; display: none'>Last update: " + Date() + "</li>").appendTo("#castList li:last-child").fadeIn(2000);

}


$(document).ready(function(){

	renderNextItem();

});

