var mongoose = require("mongoose");
mongoose.set('useCreateIndex', true);

var slugify = require('slugify');
var slugifyOptions = {
  replacement: '-',
  remove: /[*+~.()'"!:@]/g,
  lower: true
};

mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/Achievements", { useNewUrlParser: true });

var Schema = mongoose.Schema;

// ********* Models ******** //


/////////// C O M P A N Y ///////////
var companySchema = new Schema({
	name : {
		type: String,
		required: true,
		unique: true,
		trim: true		
	},
	address : {
		type: String,
		required: true,
		trim: true
	},
	country : {
		type: String,
		required: true
	},
	city : {
		type: String,
		required: true,
		trim: true
	},
	state : {
		type: String,
		required: true,
		trim: true
	},
	zip : {
		type: String,
		required: true,
		trim: true
	},
	users: [{
			type: Schema.Types.ObjectId,
			ref: "User"
		}],
	games: [{
			type: Schema.Types.ObjectId,
			ref: "Game"
		}],
	url: {
		type: String
	},
});

companySchema.methods.addUrl = function(){
	var url = slugify(this.name, slugifyOptions);

	this.set({ url: url });
	this.save(function (err, game) {
	if (err) return err;
	});
}

var Company = mongoose.model("Company", companySchema);

exports.Company = Company;

/////////// U S E R ///////////
var userSchema = new Schema({
	name : {
		type: String,
		required: true,
		trim: true		
	},
	userName : {
		type: String,
		required: true,
		trim: true		
	},
	//// Check this.
	/*email : {
		type: String,
		required: true,
		unique: true,
		lowercase: true,
		//match: regexp usar módulo para validar
		trim: true
	},*/
	password : {
		type: String,
		required: true
		// Validation!
	},
	
	admin: {
		type: Boolean
	},
	dataEntry: {
		type: Boolean
	},
	marketing: {
		type: Boolean
	},
	company: {
		type: Schema.Types.ObjectId,
		ref: "Company"
	}

});

var User = mongoose.model("User", userSchema);

exports.User = User;

/////////// G A M E  ///////////

var gameSchema = new Schema ({
	name: {
		type: String
	},
	url: {
		type: String
	},
	release: {
		type: Date
	},
	genre: {
		type: Array
	},
	plataform: {
		type: Array
	},
	gameMode: {
		type: Array
	},
	price: {
		type: Number
	},
	costs: {
		type: Number
	},
	sales: {
		type: Schema.Types.ObjectId,
		ref: "Sales"
	},
	totalSold: {
		type: Number
	},
	earnings: {
		type: Number
	},
	description: {
		type: String
	},
	cast: {
		"Directed by": {
			type: String
		},
		"Written by": {
			type: String
		},
		"Stars": {
			type: String
		},
		"Music": {
			type: String
		},
		"Art Direction": {
			type: String
		}
	},
	videoURL: {
		type: String
	},
	company: {
		//default: req.session.user.company._id, ***** here or in the server???
		type: Schema.Types.ObjectId,
		ref: "Company"
	}	

});

gameSchema.methods.addUrl = function(){
	var url = slugify(this.name, slugifyOptions);

	this.set({ url: url });
	this.save(function (err, game) {
	if (err) return handleError(err);
	});
};

gameSchema.methods.addTotalSold = function(){

	var sold = (Object.values(this.sales.sold)[0]);

	console.log(Object.keys(this.sales.sold).length);

	for (var i = 1; i < Object.keys(this.sales.sold).length; i++) {
		
		sold = sold + (Object.values(this.sales.sold)[i]);
		console.log(sold);

	};

		this.set({ totalSold: sold });
		this.set({ earnings: this.totalSold * this.price});

};


var Game = mongoose.model("Game", gameSchema);
exports.Game = Game;


/////////// S A L E S ///////////

var salesSchema = new Schema({
	game: {
		type: Schema.Types.ObjectId,
		ref: "Game"
	},
	sold: {}
});

var Sales = mongoose.model("Sales", salesSchema);
exports.Sales = Sales;
/////////// G E N R E ///////////

var genreSchema = new Schema({
	name: {
		type: String
	},
	game: [{
		type: Schema.Types.ObjectId,
		ref: "Game"
	}]
});

var Genre = mongoose.model("Genre",genreSchema);
exports.Genre = Genre;

/////////// P L A T A F O R M ///////////

var plataformSchema = new Schema({
	name: {
		type: String
	},
	game: [{
		type: Schema.Types.ObjectId,
		ref: "Game"
	}]
});

var Plataform = mongoose.model("Plataform", plataformSchema);
exports.Plataform = Plataform;

/////////// G A M E   M O D E ///////////

var gameModeSchema = new Schema({
	name: {
		type: String
	},
	game: [{
		type: Schema.Types.ObjectId,
		ref: "Game"
	}]
});

var GameMode = mongoose.model("GameMode",gameModeSchema);
exports.GameMode = GameMode;

